#singleinstance force
::test contract::
    Random, rand, 0, 42
    send, Test Contract %rand%{Tab}
    Sleep, 500

    Random, rand, 0, 42
    send, Contract Partner %rand%{Tab}
    Sleep, 500

    Random, rand, 0, 42
    send, Project %rand%{Tab}{Tab}
    Sleep, 500

    Random, rand, 1, 10
    Loop %rand% {
        send, {Down}
        Sleep, 100
    }
    send, {Enter}
    Sleep, 500
    send, {Tab}

    Random, rand, 1, 12
    Loop %rand% {
        send, {Down}
        Sleep, 100
    }
    send, {Enter}
    Sleep, 500
    send, {Tab}

    Random, rand, 1, 3
    Loop %rand% {
        send, {Down}
        Sleep, 100
    }
    send, {Enter}
    Sleep, 500
    send, {Tab}

    Random, rand, 6, 12
    send, %rand%{Tab}
    Sleep, 500

    Random, rand, 0, 5
    send, %rand%{Tab}
    Sleep, 500
    send, {Tab}{Tab}{Tab}{Tab}{Tab}{Tab}
    Sleep, 500

    Loop 8 {
        send, {Down}
        Sleep, 100
    }
    send, {Enter}
    Sleep, 500
    send, {Tab}{Tab}{Tab}{Tab}{Tab}{Tab}{Tab}{Tab}
    Sleep, 500
    send, Lars Vornholt
    Sleep, 1000
    send, {Down}{Enter}
Return