
class ScreenIdentifier {

    static instance

    GetInstance() {
        if (!instance) {
            ScreenIdentifier.instance := new ScreenIdentifier()
        }

        return ScreenIdentifier.instance
    }

    IdentifyScreens() {
        SysGet monitorCount, monitorCount
        Loop %monitorCount% {
            SysGet m, Monitor, %A_Index%
            x := mLeft + Round((mRight - mLeft) / 2)
            x = x%x%
            Gui, Monitor%A_Index%: New, +AlwaysOnTop +LastFound +Owner -Caption, Monitor %A_Index%
            Gui, Color, 000000
            WinSet, TransColor, 000000
            Gui, Font, s42 cRed
            Gui, Add, Text,, %A_Index%
            Gui, Show, %x%
        }
    }

    

}