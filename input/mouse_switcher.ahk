#SingleInstance force

class ScreenAction {
    __New(target, action) {
        this.target := target
        this.action := action
    }
}

class ScreenInfo {
    Width {
        get {
            return this.mtRight - this.mtLeft
        }
    }

    Height {
        get {
            return this.mtBottom - this.mtTop
        }
    }

    screenLock := 0

    __New(screenNr, screenCount) {
        SysGet mt, Monitor, %screenNr%
        this.screenNr := screenNr
        this.mtLeft := mtLeft + 1
        this.mtRight := mtRight - 1
        this.mtTop := mtTop + 1
        this.mtBottom := mtBottom - 1
        this.aLeft := new ScreenAction(screenNr <= 1 ? screenCount : screenNr - 1, 2)
        this.aRight := new ScreenAction(screenCount <= screenNr ? 1 : screenNr + 1, 1)
        this.aTop := new ScreenAction(screenNr, 4)
        this.aBottom := new ScreenAction(screenNr, 3)
    }

    IsInScreen(x, y) {
        if (this.screenLock < A_TickCount) {
            this.screenLock := A_TickCount + 200
            iis := x > this.mtLeft And x < this.mtRight And y > this.mtTop And y < this.mtBottom
            cs := this.screenNr
            ToolTip, S(%cs%) %x% - %y% = %iis%
        }

        return x > this.mtLeft And x < this.mtRight And y > this.mtTop And y < this.mtBottom
    }

    GetAction(x, y) {
        if (x <= this.mtLeft) {
            return this.aLeft
        } else if (x >= this.mtRight) {
            return this.aRight
        } else if (y <= this.mtTop) {
            return this.aTop
        } else if (y >= this.mtBottom) {
            return this.aBottom
        }
    }

    SetToBoundary(direction, ByRef x, ByRef y, origWidth, origHeight) {
        global currentScreen
        ;MsgBox, %direction%, %x%, %y%, %origWidth%, %origHeight%
        if (direction = 1) {
            x := this.mtLeft + 5
            y := this.Height * (1 / origHeight * y)
            h := this.Height
            p := (1 / origHeight * y)
            sn := this.screenNr
            currentScreen := this.screenNr
            ;MsgBox, %sn% %x% %h%, %y%, %p%
        } else if (direction = 2) {
            x := this.mtRight - 5
            y := this.Height * (1 / origHeight * y)
        } else if (direction = 3) {
            x := this.Width * (1 / origWidth * x)
            y := this.mtTop + 5
        } else if (direction = 4) {
            x := this.Width * (1 / origWidth * x)
            y := this.mtBottom - 5
        }
    }
}

SetBatchLines -1
SetMouseDelay -1        ; fastest action
CoordMode Mouse, Screen ; screen coordinates

screens := []
IndexScreens()
MouseGetPos, xPos, yPos

currentScreen := GetCurrentScreen(xPos, yPos)
ySpeed = 2         ; up-down movement
xSpeed = 2       ; larger number = faster movement

DllCall("SetWindowsHookEx",Int,14, Uint,RegisterCallback("Mouse","F") ; WH_MOUSE_LL = 14
       , UInt,DllCall("GetModuleHandle",UInt,0), UInt,0)

Mouse(nCode, wParam, lParam) { ; low-level mouse handler
   Global
   Static x0, y0
   If x0 =
      MouseGetPos, x0, y0
   If (wParam = 0x200) {
        x := NumGet(lParam+0)
        y := NumGet(lParam+4)
        dx := Round((x0-x))
        dy := Round((y0-y))
        newX := x0 - dx
        newY := y0 - dy
        ;MsgBox, %newX% %newY%
        CalculateScreenBoundaries(newX, newY)
        MouseMove newX, newY, 0
        x0 := newX, y0 := newY
    
        Return 1
   }
   Return DllCall("CallNextHookEx", UInt,0, Int,nCode, UInt,wParam, UInt,lParam)
}

CalculateScreenBoundaries(ByRef x, ByRef y) {
    Global screens, currentScreen  
    
    if (currentScreen = -1) {
        MsgBox, Resetting
        x := 50
        y := 50
        currentScreen := 1
    }
    if (!screens[currentScreen].IsInScreen(x, y)) {
        action := screens[currentScreen].GetAction(x, y)
        if (action And action.action != 0) {
            cs := screens[currentScreen]
            ts := action.target
            screens[action.target].SetToBoundary(action.action, x, y, cs.Width, cs.Height)
        }
    }
}

IndexScreens() {
    global screens, screenCount
    SysGet screenCount, MonitorCount

    Loop %screenCount% {
        screens.InsertAt(A_Index, new ScreenInfo(A_Index, screenCount))
    }
}

GetCurrentScreen(xPos, yPos) {
    global screens, screenCount
    Loop %screenCount% {
        s := screens[A_Index]
        isInScreen := s.IsInScreen(xPos, yPos)
        if (s.IsInScreen(xPos, yPos)) {
            return A_Index
        }
    }
    return -1
}

!z::ExitApp