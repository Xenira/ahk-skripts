﻿#SingleInstance, force
#Persistent
#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
Menu, Tray, Icon, stundenzettel.ico
logDir = %A_ScriptDir%\logs
FileCreateDir, %logDir%


lastActivity := ""
activity := ""
intervall := 15 * 60 * 1000

SetTimer, statusUpdate, %intervall%
Goto, statusUpdate
return

statusUpdate:
timestamp := getRoundedTimestamp()
InputBox, activity, "What are you doing?",,,,,,,, 30, %activity%
if (activity != lastActivity)
{
    FileAppend,
    (
%timestamp%    %activity%

    ), %logDir%\%A_YYYY%-%A_MM%-%A_DD%_%A_DDDD%.log
}
lastActivity := activity
return


getRoundedTimestamp() {
    h := A_Hour * 100
    r := Mod(A_Min, 15)
    if (r <= 7) {
        return h + A_Min - r
    } else {
        m := A_Min + (15 - r)
        if (m == 60) {
            m := 0
            h += 100
        }
        return h * 100 + m
    }
}